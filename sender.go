package gcm

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

const (
	GCMSendEndpoint     = "https://android.googleapis.com/gcm/send"
	BackoffInitialDelay = time.Second
	Authorization       = "Authorization"
	ContentType         = "Content-Type"
	JsonType            = "application/json"
)

type Sender struct {
	apiKey string
	http   *http.Client
}

func New(key string) *Sender {
	return &Sender{
		apiKey: key,
		http:   new(http.Client),
	}
}

func NewAppEngine(key string, client *http.Client) *Sender {
	return &Sender{
		apiKey: key,
		http:   client,
	}
}

func (s *Sender) Send(message *Message, retry int) (*Response, error) {
	for i := 0; i < retry; i++ {
		if resp, err := s.SendNoRetry(message); err == nil {
			return resp, nil
		}
		time.Sleep(time.Duration(i*2) * BackoffInitialDelay)
	}
	return nil, errors.New("Could not send message after " + strconv.Itoa(retry) + " attempts")
}

func (s *Sender) SendNoRetry(message *Message) (resp *Response, err error) {
	data, err := json.Marshal(message)
	if err != nil {
		return
	}

	resp, err = s.post(GCMSendEndpoint, JsonType, bytes.NewReader(data))
	return
}

func (s *Sender) post(url string, bodyType string, body io.Reader) (ret *Response, err error) {
	request, err := http.NewRequest("POST", url, body)
	if err != nil {
		return
	}
	request.Header.Set(Authorization, fmt.Sprintf("key=%s", s.apiKey))
	request.Header.Set(ContentType, bodyType)

	resp, err := s.http.Do(request)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("%s %s", resp.Status, string(respBody))
	}

	ret = new(Response)
	err = json.Unmarshal(respBody, ret)
	return
}
