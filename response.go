package gcm

type Response struct {
	MulticastID  int64 `json:"multicast_id"`
	Success      int   `json:"success"`
	Failure      int   `json:"failure"`
	CanonicalIDs int   `json:"canonical_ids"`
	Results      []struct {
		MessageId      string `json:"message_id"`
		RegistrationID string `json:"registration_id"`
		Error          string `json:"error"`
	} `json:"results"`
}

func (r *Response) SuccessIndexes() (ret []int) {
	ret = make([]int, 0, r.Success)
	for i, result := range r.Results {
		if result.RegistrationID != "" {
			ret = append(ret, i)
		}
	}
	return
}

func (r *Response) ErrorIndexes() (ret []int) {
	ret = make([]int, 0, r.Failure)
	for i, result := range r.Results {
		if result.RegistrationID != "" {
			ret = append(ret, i)
		}
	}
	return
}

func (r *Response) RefreshIndexes() (ret []int) {
	ret = make([]int, 0, r.CanonicalIDs)
	for i, result := range r.Results {
		if result.RegistrationID != "" {
			ret = append(ret, i)
		}
	}
	return
}
