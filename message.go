package gcm

type Message struct {
	RegistrationIDs []string          `json:"registration_ids"`
	CollapseKey     string            `json:"collapse_key,omitempty"`
	DelayWhileIdle  bool              `json:"delay_while_idle,omitempty"`
	Data            map[string]string `json:"data"`
	TimeToLive      int               `json:"time_to_live,omitempty"`
}

func NewMessage(ids []string) *Message {
	return &Message{
		RegistrationIDs: ids,
		Data:            make(map[string]string),
	}
}

func (m *Message) AddRecipient(ids ...string) {
	m.RegistrationIDs = append(m.RegistrationIDs, ids...)
}

func (m *Message) SetPayload(key, value string) {
	if m.Data == nil {
		m.Data = make(map[string]string)
	}
	m.Data[key] = value
}
