GCM
===

Google Cloud Message

Getting Started
---------------
```go
import "bitbucket.org/mirwing/gcm"
```

Sample Usage
------------

```go
package main

import (
	"net/http"

	"bitbucket.org/mirwing/gcm"
)

func main() {
}
```

Google AppEngine
-------------------------------

```go
package sample

import (
	"appengine"
	"appengine/urlfetch"

	"bitbucket.org/mirwing/gcm"
)

func handler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	client := urlfetch.Client(c)
	sender := gcm.NewAppEngine("api_key", client)

	//...
}
```

### Documentation

http://d.android.com/google/gcm/

https://code.google.com/p/gcm/
